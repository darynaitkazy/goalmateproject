package com.goal.goalmate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoalmateApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoalmateApplication.class, args);
	}

}
