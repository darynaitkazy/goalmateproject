package com.goal.goalmate.payload.response;

import com.goal.goalmate.models.User;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class CheckFollowingResponse {
    private User follower;
    private User followed;
    private Boolean isFollowed;
}
