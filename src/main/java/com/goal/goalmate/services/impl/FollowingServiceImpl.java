package com.goal.goalmate.services.impl;

import com.goal.goalmate.models.Contract;
import com.goal.goalmate.models.Follow;
import com.goal.goalmate.models.User;
import com.goal.goalmate.repositories.FollowingRepository;
import com.goal.goalmate.services.FollowingService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FollowingServiceImpl implements FollowingService {

    private final FollowingRepository followingRepository;

    public FollowingServiceImpl(FollowingRepository followingRepository) {
        this.followingRepository = followingRepository;
    }

    @Override
    public Follow createFollowing(User follower, User followed) {
        Follow follow = new Follow();
        follow.setFollower(follower);
        follow.setFollowed(followed);
        return followingRepository.save(follow);
    }

    @Override
    public void deleteFollowing(User follower, User followed) {
        Follow follow = followingRepository.findByFollowerAndFollowed(follower, followed);
        followingRepository.delete(follow);
    }

    @Override
    public List<User> findFollowingUsers(User follower) {
        List<Follow> followings = followingRepository.findByFollower(follower);
        return followings.stream()
                .map(Follow::getFollowed)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findFollowedUsers(User followed) {
        List<Follow> followings = followingRepository.findByFollowed(followed);
        return followings.stream()
                .map(Follow::getFollower)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contract> getContractsOfFollwings(List<User> users) {
        return null;
    }

    @Override
    public boolean checkFollowing(User follower, User followed) {
        Follow follow = followingRepository.findByFollowerAndFollowed(follower, followed);
        if (follow != null) return true;
        else return false;
    }

}
