package com.goal.goalmate.repositories;

import com.goal.goalmate.models.Follow;
import com.goal.goalmate.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface FollowingRepository extends JpaRepository<Follow, UUID> {
    List<Follow> findByFollower(User user);

    List<Follow> findByFollowed(User user);

    //@Query(value = "SELECT * FROM followings u WHERE u.follower = ?1 AND u.followed = ?2", nativeQuery = true)
    Follow findByFollowerAndFollowed (User userFrom, User userTo);

}
