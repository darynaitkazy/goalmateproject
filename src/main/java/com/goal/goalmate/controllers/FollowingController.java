package com.goal.goalmate.controllers;

import com.goal.goalmate.payload.response.CheckFollowingResponse;
import com.goal.goalmate.models.User;
import com.goal.goalmate.repositories.UserRepository;
import com.goal.goalmate.services.FollowingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class FollowingController {
    @Autowired
    private FollowingService followingService;
    @Autowired
    private UserRepository userRepository;

    //GET api for getting all followings of a user
    @GetMapping("/{userId}/followings")
    public ResponseEntity<?> getFollowings(@PathVariable(value = "userId") UUID userId) {
        Optional<User> user = userRepository.findById(userId);
        List<User> followings = followingService.findFollowingUsers(user.get());
        return ResponseEntity.ok(followings);
    }
    //GET api for getting all followers of a user
    @GetMapping("/{userId}/followed")
    public ResponseEntity<?> getFollowedUsers(@PathVariable(value = "userId") UUID userId) {
        Optional<User> user = userRepository.findById(userId);
        List<User> followedUsers = followingService.findFollowedUsers(user.get());
        return ResponseEntity.ok(followedUsers);
    }
    //POST api for follow button
    @PostMapping("/follow")
    public ResponseEntity<?> createFollowing (@RequestParam(value = "userToId") String userToId,
                                              @RequestParam(value = "userId") String userId) {
        Optional<User> userFrom = userRepository.findById(UUID.fromString(userId));
        Optional<User> userTo = userRepository.findById(UUID.fromString(userToId));
        return ResponseEntity.ok(followingService.createFollowing(userFrom.get(), userTo.get()));
    }
    //DELETE api for delete following
    @PostMapping("/unfollow")
    public ResponseEntity<?> deleteFollowing (@RequestParam(value = "userToId") String userToId,
                                              @RequestParam(value = "userId") String userId) {
        Optional<User> userFrom = userRepository.findById(UUID.fromString(userId));
        Optional<User> userTo = userRepository.findById(UUID.fromString(userToId));
        followingService.deleteFollowing(userFrom.get(), userTo.get());
        return ResponseEntity.ok("Following successfully deleted");
    }
    //GET api check if current user is already followed
    @GetMapping("/check-following")
    public ResponseEntity<?> checkFollowing (@RequestParam(value = "userToId") String userToId,
                                             @RequestParam(value = "userId") String userId) {
        Optional<User> userFrom = userRepository.findById(UUID.fromString(userId));
        Optional<User> userTo = userRepository.findById(UUID.fromString(userToId));
        CheckFollowingResponse checkFollowingResponse = new CheckFollowingResponse();
        checkFollowingResponse.setFollower(userFrom.get());
        checkFollowingResponse.setFollowed(userTo.get());
        if (followingService.checkFollowing(userFrom.get(), userTo.get())) {
            checkFollowingResponse.setIsFollowed(true);
            return ResponseEntity.ok(checkFollowingResponse);
        } else {
            checkFollowingResponse.setIsFollowed(false);
            return ResponseEntity.ok(checkFollowingResponse);
        }
    }
}
